import com.heroicrobot.dropbit.registry.*;
import com.heroicrobot.dropbit.devices.pixelpusher.Pixel;
import com.heroicrobot.dropbit.devices.pixelpusher.Strip;
import java.util.*;

DeviceRegistry registry;

class TestObserver implements Observer {
  public boolean hasStrips = false;
  public void update(Observable registry, Object updatedDevice) {
    println("Registry changed!"); 
    if (updatedDevice != null) {
      println("Device change: " + updatedDevice);
    }
    this.hasStrips = true;
  }
}

TestObserver testObserver;
int colorstep, heightstep, widthstep;
float diameter;
color[] pix;

void setup() {
  registry = new DeviceRegistry();
  testObserver = new TestObserver();
  registry.addObserver(testObserver);
  registry.setAntiLog(true);
  registry.setLogging(false);
  registry.setAutoThrottle(false);
  
  frameRate(30);
  size(528, 65);
  pix = new color[240];
  
  colorstep = 0;
  heightstep = 0;
  widthstep = 0;
  
  colorMode(HSB, 255);
  
  fill(0, 255);
  rect(0, 0, width, height);
  
  diameter = 40;
}

void draw() {
  
  colorMode(HSB, 255);
  
  fill(0, 5);
  rect(0, 0, width, height);
  
  colorstep = (colorstep + 1) % 256;
  heightstep = (heightstep + 1) % height;
  widthstep = (widthstep + 1) % width;
  
    
  fill(colorstep, 255, 255, 255);
  ellipse(widthstep, heightstep, diameter, diameter);
  
  fill(255 - colorstep, 255, 255, 255);
  ellipse((widthstep + width/2) % width, (heightstep + height/2) % height, diameter, diameter);
  
  fill(255 - colorstep, 255, 255, 255);
  ellipse(width - widthstep, height - heightstep, diameter, diameter);
  
  fill(colorstep, 255, 255, 255);
  ellipse(width - (widthstep + width/2) % width, height - (heightstep + height/2) % height, diameter, diameter);
  
  if (testObserver.hasStrips) {   
    registry.startPushing();
    List<Strip> strips = registry.getStrips();
    int numStrips = strips.size();
    if (numStrips == 0) return;
    
    colorMode(RGB, 255);
    
    for (Strip strip : strips) {
      
      int stripx = 0;
      for (int i = 55; i >= 0; i--) {
        PImage section = get(8*(i+4), 5, 8, 10);
        
        long avgRed = 0;
        long avgGreen = 0;
        long avgBlue = 0;
        long numPixels = section.pixels.length;
        for (int j = 0; j < numPixels; j++) {
          avgRed += red(section.pixels[j]);
          avgGreen += green(section.pixels[j]);
          avgBlue += blue(section.pixels[j]);
        }
        strip.setPixel(color(avgRed/numPixels, avgGreen/numPixels, avgBlue/numPixels), stripx);
        stripx++;
      }
      
      for (int i = 63; i >= 0; i--) {
        PImage section = get(8*(i+2), 20, 8, 10);
        
        long avgRed = 0;
        long avgGreen = 0;
        long avgBlue = 0;
        long numPixels = section.pixels.length;
        for (int j = 0; j < numPixels; j++) {
          avgRed += red(section.pixels[j]);
          avgGreen += green(section.pixels[j]);
          avgBlue += blue(section.pixels[j]);
        }
        strip.setPixel(color(avgRed/numPixels, avgGreen/numPixels, avgBlue/numPixels), stripx);
        stripx++;
      }
      
      for (int i = 63; i >= 0; i--) {
        PImage section = get(8*(i), 35, 8, 10);
        
        long avgRed = 0;
        long avgGreen = 0;
        long avgBlue = 0;
        long numPixels = section.pixels.length;
        for (int j = 0; j < numPixels; j++) {
          avgRed += red(section.pixels[j]);
          avgGreen += green(section.pixels[j]);
          avgBlue += blue(section.pixels[j]);
        }
        strip.setPixel(color(avgRed/numPixels, avgGreen/numPixels, avgBlue/numPixels), stripx);
        stripx++;
      }
      
      for (int i = 55; i >= 0; i--) {
        PImage section = get(8*(i+5), 50, 8, 10);
        
        long avgRed = 0;
        long avgGreen = 0;
        long avgBlue = 0;
        long numPixels = section.pixels.length;
        for (int j = 0; j < numPixels; j++) {
          avgRed += red(section.pixels[j]);
          avgGreen += green(section.pixels[j]);
          avgBlue += blue(section.pixels[j]);
        }
        strip.setPixel(color(avgRed/numPixels, avgGreen/numPixels, avgBlue/numPixels), stripx);
        stripx++;
      }
    }
    
    
  }
}